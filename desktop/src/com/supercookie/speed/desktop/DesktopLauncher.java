package com.supercookie.speed.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.supercookie.speed.SpeedGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Death Race 2000";
        config.width = 1280;
        config.height = 960;
		new LwjglApplication(new SpeedGame(), config);
	}
}
