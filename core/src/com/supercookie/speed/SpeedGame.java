package com.supercookie.speed;

import com.badlogic.gdx.Game;

public class SpeedGame extends Game {
    public static final int GAME_VIEWPORT_WIDTH = 1024, GAME_VIEWPORT_HEIGHT = 768;

    @Override
    public void create() {
        setScreen(new GameScreen());
    }
}
