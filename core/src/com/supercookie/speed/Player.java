package com.supercookie.speed;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by Cookie on 03/05/2014.
 */
public class Player {


    private static final Texture CAR_STRAIGHT = new Texture("player_straight.png");

    private float x = 0;
    private float y = 0;

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public int getWidth() {
        return CAR_STRAIGHT.getWidth();
    }

    public int getHeight() {
        return CAR_STRAIGHT.getHeight();
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void draw(Batch batch, float x, float y, float width, float height) {
        draw(batch, x, y, width, height, 1);
    }

    public void draw(Batch batch, float x, float y, float width, float height, float percentageToDraw) {
        batch.draw(CAR_STRAIGHT, x, y, width, height);
    }
}
