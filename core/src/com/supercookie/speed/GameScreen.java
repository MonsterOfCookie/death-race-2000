package com.supercookie.speed;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.supercookie.speed.road.Road;
import com.supercookie.speed.road.impl.SimpleRoad;

/**
 * Created by James on 11/03/2015.
 */
public class GameScreen extends ScreenAdapter {

    private final Stage stage = new Stage(new FitViewport(SpeedGame.GAME_VIEWPORT_WIDTH, SpeedGame.GAME_VIEWPORT_HEIGHT));
    private final BitmapFont bitmapFont = new BitmapFont();
    private final Label stats = new Label("", new Label.LabelStyle(bitmapFont, Color.WHITE));
    private final Road road = new SimpleRoad();

    @Override
    public void show() {
        super.show();
        stats.setFontScale(2F);
        stage.addActor(stats);
        road.resetRoad();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        GL20 gl = Gdx.graphics.getGL20();
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        road.update(delta);
        road.render(stage.getCamera());
        stats.setText(Gdx.graphics.getFramesPerSecond() + road.stats());
        stats.setPosition(0, SpeedGame.GAME_VIEWPORT_HEIGHT - stats.getTextBounds().height);

        stage.act(delta);
        stage.draw();
    }
}
