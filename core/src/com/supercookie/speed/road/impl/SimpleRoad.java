package com.supercookie.speed.road.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.supercookie.speed.Player;
import com.supercookie.speed.SpeedGame;
import com.supercookie.speed.road.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by James on 11/03/2015.
 */
public class SimpleRoad implements Road {

    public static final float WIDTH = SpeedGame.GAME_VIEWPORT_WIDTH;
    public static final float HEIGHT = SpeedGame.GAME_VIEWPORT_HEIGHT;

    private static final float ROAD_WIDTH = 2000;
    private static final float CAMERA_HEIGHT = 1000;
    private static final float FIELD_OF_VIEW = 100;
    private static final int DRAW_DISTANCE = 500;
    private static final float CENTRIFUGAL = 0.2F;
    private static final float SPRITE_SCALE = 0.3F * (1 / 320F); //320 is the players car width

    private static final Texture BACKGROUND_HILLS = new Texture("hills.png");
    private static final Texture BACKGROUND_SKY = new Texture("sky.png");
    private static final Texture BACKGROUND_TREES = new Texture("trees.png");

    private static final float ROAD_SCALE_FACTOR = WIDTH / 2 * (SPRITE_SCALE * ROAD_WIDTH);
    private static final float CAMERA_DEPTH = (float) (1 / Math.tan((FIELD_OF_VIEW / 2) * Math.PI / 180));
    private static final float PLAYER_Z = CAMERA_HEIGHT * CAMERA_DEPTH;

    private PolygonSpriteBatch polygonSpriteBatch = new PolygonSpriteBatch();

    private float maxSpeed = 0;
    private float speed = 0;

    private final List<Car> cars = new ArrayList<>();
    private List<RoadSegment> roadSegments;
    private int trackLength;
    private int position = 0;

    private Player player = new Player();

    public void resetRoad() {

        RoadBuilder roadBuilder = new RoadBuilder()
                .addDip(RoadBuilder.Length.SHORT, RoadBuilder.Hills.HIGH)
                .addHill(RoadBuilder.Length.SHORT, RoadBuilder.Hills.HIGH)
                .addHill(RoadBuilder.Length.SHORT, RoadBuilder.Hills.HIGH)
                .addDip(RoadBuilder.Length.SHORT, RoadBuilder.Hills.HIGH)
                .addStraight(RoadBuilder.Length.MEDIUM)
                .addRightCurve(RoadBuilder.Length.LONG, RoadBuilder.Curve.LIGHT, RoadBuilder.Hills.NONE)
                .addLeftCurve(RoadBuilder.Length.LONG, RoadBuilder.Curve.TIGHT, RoadBuilder.Hills.NONE);

        roadSegments = roadBuilder.build();
        trackLength = roadSegments.size() * RoadSegment.SEGMENT_LENGTH;

        resetCars();
        resetScenery();
    }

    public void resetCars() {
        Car car;
        RoadSegment segment;
        float offset;
        int z;
        float speed;
        for (int n = 0; n < 25; n++) {
            offset = (float) (Math.random() * MathUtils.random(-1F, 1F));
            z = (int) (Math.floor(Math.random() * roadSegments.size()) * RoadSegment.SEGMENT_LENGTH);
            speed = 1000F + (float) (MathUtils.random(1000, 5000));
            car = new Car(offset, z, speed);
            segment = findSegment(car.getZ());
            segment.addCar(car);
            cars.add(car);
        }
    }

    public void resetScenery() {
        Scenery scenery;
        float offset;
        for (int n = 0; n < roadSegments.size(); n++) {
            if (MathUtils.randomBoolean(0.5F)) {
                RoadSegment roadSegment = roadSegments.get(n);
                offset = MathUtils.randomBoolean() ? -1F - (float) (Math.random() * 1F) : 1F + (float) (Math.random() * 1F);
                scenery = new Scenery(offset, 0);
                roadSegment.addScenery(scenery);
            }
        }
    }

    public RoadSegment findSegment(int z) {
        return roadSegments.get(MathUtils.floor(z / RoadSegment.SEGMENT_LENGTH) % roadSegments.size());
    }

    private final StringBuilder stats = new StringBuilder();

    @Override
    public String stats() {
        return stats.toString();
    }

    public void update(float delta) {
        stats.setLength(0);

        maxSpeed = RoadSegment.SEGMENT_LENGTH / delta;

        float accel = maxSpeed / 5;             // acceleration rate - tuned until it 'felt' right
        float braking = -maxSpeed;               // deceleration rate when braking
        float decel = -maxSpeed / 5;             // 'natural' deceleration rate when neither accelerating, nor braking
        float offRoadDecel = -maxSpeed / 2;             // off road deceleration is somewhere in between
        float offRoadLimit = maxSpeed / 4;          // limit when off road deceleration no longer applies (e.g. you can always go at least this speed even when off road)

        float playerX = player.getX();
        float playerW = player.getWidth() * SPRITE_SCALE;

        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            speed = accelerate(speed, accel, delta);
        } else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            speed = accelerate(speed, braking, delta);
        } else {
            speed = Math.max(0,accelerate(speed, decel, delta));
        }

        position = Math.round(increase(position, delta * speed, trackLength));
        RoadSegment playerSegment = findSegment(Math.round(position + PLAYER_Z));

        updateCars(delta, playerSegment, playerW);

        if (speed > 0) {

            float speedPercent = speed / maxSpeed;
            float dx = delta * speedPercent * 2; // at top speed, should be able to cross from left to right (-1 to 1) in 1 second
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                playerX = playerX - dx;
            } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                playerX = playerX + dx;
            }

            playerX = playerX - (dx * speedPercent * playerSegment.getCurve() * CENTRIFUGAL);

            if (((playerX < -1) || (playerX > 1)) && (speed > offRoadLimit)) {
                speed = accelerate(speed, offRoadDecel, delta);
                for (int n = 0; n < playerSegment.getScenery().size(); n++) {
                    Scenery scenery = playerSegment.getScenery().get(n);
                    float sceneryW = scenery.getWidth() * SPRITE_SCALE;
                    float sceneryX = scenery.getOffset() + sceneryW / 2 * (scenery.getOffset() > 0 ? 1 : -1);
                    if (overlap(playerX, playerW, sceneryX, sceneryW)) {
                        speed = maxSpeed / 100;
                        position = Math.round(increase(playerSegment.getP1().world.z, -PLAYER_Z, trackLength)); // stop in front of sprite (at front of segment)
                        break;
                    }
                }
            }

            for (int n = 0; n < playerSegment.getCars().size(); n++) {
                Car car = playerSegment.getCars().get(n);
                float carW = car.getWidth() * SPRITE_SCALE;
                if (speed > car.getSpeed()) {
                    if (overlap(playerX, playerW, car.getOffset(), carW, 0.8F)) {
                        speed = car.getSpeed() * (car.getSpeed() / speed);
                        position = Math.round(increase(car.getZ(), -PLAYER_Z, trackLength));
                        break;
                    }
                }
            }

            playerX = MathUtils.clamp(playerX, -2, 2);
            speed = MathUtils.clamp(speed, 0, maxSpeed);
            player.setPosition(playerX, 0);
        } else {
            speed = 0;
        }

        stats.append("\n");
        stats.append("P Pos: ").append(player.getX()).append("x").append(player.getY()).append("\n");
        stats.append("P Spd: ").append(speed).append(" M Spd: ").append(maxSpeed).append("\n");
        stats.append("R Pos: ").append(position).append("\n");
        stats.append("Segs : ").append(roadSegments.size()).append(" Current ").append(findSegment(position).getIndex()).append("\n");
    }

    private void updateCars(float delta, RoadSegment playerSegment, float playerW) {
        Car car;
        RoadSegment oldSegment;
        RoadSegment newSegment;
        for (int n = 0; n < cars.size(); n++) {
            car = cars.get(n);
            oldSegment = findSegment(car.getZ());
            float offset = car.getOffset() + updateCarOffset(car, oldSegment, playerSegment, playerW);
            int z = Math.round(increase(car.getZ(), delta * car.getSpeed(), trackLength));
            car.updateCar(z, offset);
            newSegment = findSegment(car.getZ());
            if (oldSegment != newSegment) {
                oldSegment.getCars().remove(car);
                newSegment.getCars().add(car);
            }
        }
    }


    private float updateCarOffset(Car car, RoadSegment carSegment, RoadSegment playerSegment, float playerW) {

        float dir;
        RoadSegment segment;
        Car otherCar;
        float otherCarW;
        int lookahead = 20;
        float carW = car.getWidth() * SPRITE_SCALE;
        float playerX = player.getX();

        // optimization, dont bother steering around other cars when 'out of sight' of the player
        if ((carSegment.getIndex() - playerSegment.getIndex()) > DRAW_DISTANCE)
            return 0;

        for (int i = 1; i < lookahead; i++) {
            segment = roadSegments.get((carSegment.getIndex() + i) % roadSegments.size());

            if ((segment == playerSegment) && (car.getSpeed() > speed) && (overlap(playerX, playerW, car.getOffset(), carW, 1.2F))) {
                if (playerX > 0.5)
                    dir = -1;
                else if (playerX < -0.5)
                    dir = 1;
                else
                    dir = (car.getOffset() > playerX) ? 1 : -1;
                return dir * 1 / i * (car.getSpeed() - speed) / maxSpeed; // the closer the cars (smaller i) and the greated the speed ratio, the larger the offset
            }

            for (int j = 0; j < segment.getCars().size(); j++) {
                otherCar = segment.getCars().get(j);
                otherCarW = otherCar.getWidth() * SPRITE_SCALE;
                if ((car.getSpeed() > otherCar.getSpeed()) && overlap(car.getOffset(), carW, otherCar.getOffset(), otherCarW, 1.2F)) {
                    if (otherCar.getOffset() > 0.5)
                        dir = -1;
                    else if (otherCar.getOffset() < -0.5)
                        dir = 1;
                    else
                        dir = (car.getOffset() > otherCar.getOffset()) ? 1 : -1;
                    return dir * 1 / i * (car.getSpeed() - otherCar.getSpeed()) / maxSpeed;
                }
            }
        }

        // if no cars ahead, but I have somehow ended up off road, then steer back on
        if (car.getOffset() < -0.9)
            return 0.1F;
        else if (car.getOffset() > 0.9)
            return -0.1F;
        else
            return 0;
    }

    public void render(Camera camera) {
        polygonSpriteBatch.setProjectionMatrix(camera.combined);
        polygonSpriteBatch.begin();

        polygonSpriteBatch.draw(BACKGROUND_SKY, 0, 0, BACKGROUND_SKY.getWidth(), HEIGHT);
        polygonSpriteBatch.draw(BACKGROUND_HILLS, 0, 0, BACKGROUND_HILLS.getWidth(), HEIGHT);
        polygonSpriteBatch.draw(BACKGROUND_TREES, 0, 0, BACKGROUND_TREES.getWidth(), HEIGHT);

        RoadSegment playerSegment = findSegment(Math.round(position + PLAYER_Z));
        RoadSegment baseSegment = findSegment(position);

        float playerPercent = percentRemaining(position + PLAYER_Z, RoadSegment.SEGMENT_LENGTH);
        float playerY = interpolate(playerSegment.getP1().world.y, playerSegment.getP2().world.y, playerPercent);

        float maxY = 0;
        float x = 0;
        float basePercent = (position % RoadSegment.SEGMENT_LENGTH) / (float) RoadSegment.SEGMENT_LENGTH;
        float dx = -(baseSegment.getCurve() * basePercent);

        for (int n = 0; n < Math.min(DRAW_DISTANCE, roadSegments.size()); n++) {
            RoadSegment segment = roadSegments.get((baseSegment.getIndex() + n) % roadSegments.size());
            segment.setClip(maxY);
            boolean segmentLooped = segment.getIndex() < baseSegment.getIndex();
            project(segment.getP1(), player.getX() * ROAD_WIDTH - x, playerY + CAMERA_HEIGHT, position - (segmentLooped ? trackLength : 0), CAMERA_DEPTH, WIDTH, HEIGHT, ROAD_WIDTH);
            project(segment.getP2(), player.getX() * ROAD_WIDTH - x - dx, playerY + CAMERA_HEIGHT, position - (segmentLooped ? trackLength : 0), CAMERA_DEPTH, WIDTH, HEIGHT, ROAD_WIDTH);

            x = x + dx;
            dx = dx + segment.getCurve();

            if (
                    segment.getP1().camera.z <= CAMERA_DEPTH // Behind us
                            || (segment.getP2().screen.y <= segment.getP1().screen.y)  // back face cull
                            || (segment.getP2().screen.y < maxY)) { // clip by (already rendered) segment
                continue;
            }
            segment.draw(polygonSpriteBatch);
            maxY = segment.getP1().screen.y;
        }

        // back to front painters algorithm
        for (int n = (Math.min(DRAW_DISTANCE, roadSegments.size()) - 1); n > 0; n--) {

            RoadSegment segment = roadSegments.get((baseSegment.getIndex() + n) % roadSegments.size());
//            render roadside sprites
            for (int i = 0; i < segment.getScenery().size(); i++) {
                Scenery scenery = segment.getScenery().get(i);
                float spriteScale = segment.getP1().screen.scale;
                float spriteX = segment.getP1().screen.x + (spriteScale * scenery.getOffset() * ROAD_WIDTH * WIDTH / 2);
                float spriteY = segment.getP1().screen.y;
                renderScenery(scenery, spriteScale, spriteX, spriteY, segment.getClip());
            }

            for (int i = 0; i < segment.getCars().size(); i++) {
                Car car = segment.getCars().get(i);
                float carPercent = percentRemaining(car.getZ(), RoadSegment.SEGMENT_LENGTH);
                float spriteScale = interpolate(segment.getP1().screen.scale, segment.getP2().screen.scale, carPercent);
                float spriteX = interpolate(segment.getP1().screen.x, segment.getP2().screen.x, carPercent) + (spriteScale * car.getOffset() * ROAD_WIDTH * WIDTH / 2);
                float spriteY = interpolate(segment.getP1().screen.y, segment.getP2().screen.y, carPercent);
                renderCar(car, spriteScale, spriteX, spriteY, segment.getClip());
            }

            if (segment == playerSegment) {
                renderPlayer(speed / maxSpeed, CAMERA_DEPTH / PLAYER_Z, WIDTH / 2);
            }
        }


        polygonSpriteBatch.end();
    }

    private void renderPlayer(float speedPercent, float scale, float destX) {

        float bounce = (float) (1.5 * Math.random() * speedPercent) * MathUtils.random(-1, 1);

        float destW = (player.getWidth() * scale) * ROAD_SCALE_FACTOR;
        float destH = (player.getHeight() * scale) * ROAD_SCALE_FACTOR;

        destX = destX + (destW * -0.5F);

        player.draw(polygonSpriteBatch, destX, bounce, destW, destH);
    }

    private void renderScenery(Scenery scenery, float scale, float destX, float destY, float clipY) {


        float destW = (scenery.getWidth() * scale) * ROAD_SCALE_FACTOR;
        float destH = (scenery.getHeight() * scale) * ROAD_SCALE_FACTOR;

        destX = destX + (destW * (scenery.getOffset() < 0 ? -1 : 0));

        float clipH = Math.max(0, destY + destH - clipY);
        float amountToChop = clipH / destH;
        float percentageToDraw = MathUtils.clamp(amountToChop, 0, 1);

        scenery.draw(polygonSpriteBatch, destX, destY, destW, destH, percentageToDraw);
    }

    private void renderCar(Car car, float scale, float destX, float destY, float clipY) {

        float destW = (car.getWidth() * scale) * ROAD_SCALE_FACTOR;
        float destH = (car.getHeight() * scale) * ROAD_SCALE_FACTOR;

        destX = destX + (destW * -0.5F);

        float clipH = Math.max(0, destY + destH - clipY);
        float amountToChop = clipH / destH;
        float percentageToDraw = MathUtils.clamp(amountToChop, 0, 1);

        car.draw(polygonSpriteBatch, destX, destY, destW, destH, percentageToDraw);
    }

    private void project(Point p, float cameraX, float cameraY, float cameraZ, float cameraDepth, float width, float height, float roadWidth) {
        p.camera.x = p.world.x - cameraX;
        p.camera.y = p.world.y - cameraY;
        p.camera.z = p.world.z - cameraZ;
        p.screen.scale = cameraDepth / p.camera.z;
        p.screen.x = Math.round((width / 2) + (p.screen.scale * p.camera.x * width / 2));
        p.screen.y = Math.round((height / 2) - (p.screen.scale * p.camera.y * -height / 2));
        p.screen.w = Math.round((p.screen.scale * roadWidth * width / 2));
    }


    private float increase(float start, float increment, int max) {
        float result = start + increment;
        while (result >= max) {
            result -= max;
        }
        while (result < 0) {
            result += max;
        }
        return result;
    }

    private float percentRemaining(float n, float total) {
        return (n % total) / total;
    }

    private float interpolate(float a, float b, float percent) {
        return a + (b - a) * percent;
    }

    private float accelerate(float v, float accel, float dt) {
        return v + (accel * dt);
    }

    private boolean overlap(float x1, float w1, float x2, float w2) {
        return overlap(x1, w1, x2, w2, 1);
    }

    private boolean overlap(float x1, float w1, float x2, float w2, float percent) {
        float half = percent / 2;
        float min1 = x1 - (w1 * half);
        float max1 = x1 + (w1 * half);
        float min2 = x2 - (w2 * half);
        float max2 = x2 + (w2 * half);
        return !((max1 < min2) || (min1 > max2));
    }


}

