package com.supercookie.speed.road;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by Cookie on 04/05/2014.
 */
public class Scenery {
//    private static final Texture TEXTURE = new Texture("assets/billboard01.png");
    private static final Texture TEXTURE = new Texture("boulder1.png");

    private float offset;
    private int z;

    public Scenery(float offset, int z) {
        this.offset = offset;
        this.z = z;
    }

    public float getOffset() {
        return offset;
    }

    public int getZ() {
        return z;
    }

    public int getHeight() {
        return TEXTURE.getHeight();
    }

    public int getWidth() {
        return TEXTURE.getWidth();
    }

    public void updateCar(int z, float offset) {
        this.z = z;
        this.offset = offset;
    }

    public void draw(Batch batch, float x, float y, float width, float height, float percentageToDraw) {
        batch.draw(TEXTURE, x, y + height * (1 - percentageToDraw), width, height * percentageToDraw, 0, percentageToDraw, 1, 0);
    }

    @Override
    public String toString() {
        return "Scenery{" +
                "offset=" + offset +
                ", z=" + z +
                '}';
    }
}
