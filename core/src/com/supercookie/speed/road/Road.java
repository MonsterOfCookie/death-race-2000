package com.supercookie.speed.road;

import com.badlogic.gdx.graphics.Camera;

/**
 * Created by James on 11/03/2015.
 */
public interface Road {

    void resetRoad();
    void update(float delta);
    void render(Camera camera);

    String stats();
}
